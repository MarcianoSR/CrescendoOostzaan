import React from 'react';

import colors from '../colors';

import Posts from '../containers/Posts';

import Navigation from '../components/Navigation';
import Slider from '../components/Slider';
import Title from '../components/Title';
import Button from '../components/Button';
import ContentBlock from '../components/ContentBlock';

const Home = props => (
  <div>
    <Navigation
      styles={{
        background: 'transparent',
        position: 'absolute'
      }}
    />

    <Slider image="http://www.zeldaelements.net/images/games/links_awakening/maps/worldmap.png" />
    <hr />

    <ContentBlock>
      <Title>Over Crescendo</Title>
      <p>In het pittoreske Oostzaan vindt u de Christelijke Muziekvereniging Crescendo. Dit fanfareorkest, opgericht op 1 november 1912, bestaat uit ruim veertig leden.De muzikanten van Crescendo vertegenwoordigen vele koperblaasinstrumenten. Onder leiding van dirigent Bart Koning treedt het orkest een aantal keer per jaar op bij diverse gelegenheden.</p>
      <Button>Over Crescendo</Button>
    </ContentBlock>

    <ContentBlock styles={{ background: `${colors.gray}` }}>
      <Title>Laatste Nieuws</Title>
      <Posts />
    </ContentBlock>

  </div>
);

export default Home;
