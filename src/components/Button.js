import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../colors';

const Button = (props) => {
	const ButtonElement = styled.button`
		font-size: 15px;
		text-align: center;
		padding: 20px;
		text-transform: uppercase;
		cursor: pointer;
		border: none;
	`;

	const PrimaryButton = ButtonElement.extend`
	  position: relative;
	  z-index: 1;
		color: ${colors.fountainBlue};
	  background-color: transparent;
	  border-color: ${colors.fountainBlue};
	  border-width: 1px;
		border-style: solid;
		

		&:after {
			content: ' ';
			position: absolute;
			width: 0%;
			height: 100%;
			top: 0;
			left: 0;
			z-index: -1;
			background-color: ${colors.fountainBlue};
			transition: all .3s;
		}

		&:hover, &:active {
			color:white;

			&:after {
				width:100%;
			}
		}
	`;

	if (props.type === 'secondary') {
		return <button> Secondary </button>;
	}

	return <PrimaryButton>{props.children}</PrimaryButton>;
};

Button.defaultProps = {
	type: 'primary'
};

Button.propTypes = {
	children: PropTypes.string.isRequired,
	type: PropTypes.string
};

export default Button;
