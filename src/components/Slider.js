import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const SliderContainer = styled.section`
	height: 500px;
	overflow-y: hidden;
`;

const ImageContainer = styled.figure`
	img {
		width: 100%;
		height: 100%;
	}
`;

const Slider = props => (
  <SliderContainer>
    <ImageContainer>
      <img src={props.image} alt="" />
    </ImageContainer>
  </SliderContainer>
);

Slider.propTypes = {
	image: PropTypes.string.isRequired
};

export default Slider;
