import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

// import { Button, Welcome } from '@storybook/react/demo';

import '../src/global';
import Button from '../src/components/Button';
import Title from '../src/components/Title';

// storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

// storiesOf('Button', module)
//   .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
//   .add('with some emoji', () => <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>);


storiesOf('Button', module)
  .add('Default primary', () => <Button>Over Crescendo</Button>)
  .add('With type', () => <Button type='secondary'>Over Crescendo</Button>)

storiesOf('Title', module)
  .add('Default', () => <Title>Over Crescendo</Title>)
  .add('with heading type', () => <Title heading="h2"> Over Crescendo </Title>)
  .add('with fontSize', () => <Title fontSize='20'> Over Crescendo </Title>)
  .add('with fontWeight', () => <Title fontWeight='800'> Over Crescendo </Title>)
