import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';

import styled from 'styled-components';

import './global';

import Routes from './routes';

const Wrapper = styled.div`
  background-color: white;
`;

const initializeApp = () => {
  render(
    <Wrapper>
      <Routes />
    </Wrapper>,
    document.querySelector('#root'));
};

initializeApp();
