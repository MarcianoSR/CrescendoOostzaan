import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ContentBlockElement = styled.section`
	background: ${props => props.styles.background};
	padding: 75px 50px;

	p {
		margin: 25px 0;
	}
`;

const ContentBlock = ({ styles, children }) => {
  console.log(children);
  console.log(typeof children);
  return (<ContentBlockElement styles={styles}>{children}</ContentBlockElement>);
};

ContentBlock.defaultProps = {
  styles: {
    background: 'white'
  }
};

ContentBlock.propTypes = {
  children: PropTypes.arrayOf(PropTypes.shape).isRequired,
  styles: PropTypes.shape({
    background: PropTypes.string
  })
};

export default ContentBlock;
