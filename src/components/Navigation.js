
/**
 * Navigation component.
 * @param props - type: What type of navigation bar it should be.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styled from 'styled-components';

import colors from '../colors';

/*
* Dynamic styled component: Expects props.type and changes appearance based on props.
* Note: For performance it may be better to change the arrow function.
* Every time a new template string interpolation happens, a new function is created aswell.
*/
const NavContainer = styled.nav`
	background: ${props => props.styles.background};
	position: ${props => props.styles.position};
	width: 100%;
`;

const ListContainer = styled.ul`
	list-style: none;
	display: flex;
	justify-content: flex-end;
`;

const ListItem = styled.li`
	padding: 20px;
`;

const Item = styled(Link)`
	color: white;
	text-shadow: ${colors.textShadow};
	font-size: 14px;
	text-transform: uppercase;
	font-weight: 800;
	text-decoration: none;
`;

const Navigation = ({ styles }) => (
  <NavContainer styles={styles}>
    <ListContainer>
      <ListItem><Item to="/">Home</Item></ListItem>
      <ListItem><Item to="/over-crescendo">Over Crescendo</Item></ListItem>
    </ListContainer>
  </NavContainer>
);

Navigation.defaultProps = {
	styles: {
		background: 'transparent'
	}
};

Navigation.propTypes = {
	styles: PropTypes.shape({
		background: PropTypes.string
	})
};

export default Navigation;
