// Define all needed colors.
const colors = {
	fountainBlue: '#56B8BA',
	fountainBlueLight: '#9ed6d8',
	gray: '#E6E7E8',
	white: '#fff',
	textShadow: '1px 1px 3px rgba(0, 0, 0, 0.75)'
};

export default colors;
