# Crescendo Oostzaan v2.0

## Front-end stack:
- React
- React Storybook
- Styled components
- Webpack 3

### Run dev server:

`yarn run dev:server`


### Run storybook:
`yarn run storybook`
