import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../colors';

const Title = (props) => {
  const TitleElement = styled[`${props.heading}`]`
	   color: 				${colors.fountainBlue};
	   font-weight: 	${props.fontWeight};
		 font-size: 		${props.fontSize}px
	`;

  return <TitleElement> {props.children} </TitleElement>;
};

Title.defaultProps = {
  heading: 'h1',
  fontSize: '40',
  fontWeight: '400'
};

Title.propTypes = {
  children: PropTypes.string.isRequired,
  heading: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.string
};

export default Title;
