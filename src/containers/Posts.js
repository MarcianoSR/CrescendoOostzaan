import React, { Component } from 'react';
import axios from 'axios';
import styled from 'styled-components';

import config from '../config';

import Post from '../components/Post';

const PostsContainer = styled.section`
	display: flex;
	flex-flow: row wrap;
`;

class Posts extends Component {
  constructor () {
    super();

    this.state = {
      posts: null,
      loading: true
    };

    this.fetchPosts = this.fetchPosts.bind(this);
  }

  /* componentDidMount is invoked immediately after a component is mounted.
   * Initialization that requires DOM nodes should go here.
   * If you need to load data from a remote endpoint, this is a good place to instantiate the network request.
   * Setting state in this method will trigger a re-rendering. */
  componentDidMount () {
		// Fake loading for now.
    setTimeout(() => {
      this.fetchPosts();
    }, 2000);
  }

	// Unmount component and aort the ajax request

  async fetchPosts () {
    try {
      const posts = await axios.get(`${config.apiUrl}/posts`);
			// console.log(posts);
      this.setState({ posts: posts.data, loading: false });
    }	catch (error) {
      console.log(error);
      this.setState({ posts: null, error, loading: false });
    }
  }

  render () {
    const posts = this.state.posts;
    const loading = this.state.loading;
    const error = this.state.error;

		// States:
		// 1. Fetching/Loading posts
		// 2. Showing posts
		// 3. Request was OK, but no posts to be fetched.
		// 4. Request was not OK, show error.

    let postState;

    if (loading) {
      postState = <div> De berichten worden geladen... </div>;
    }

    if (posts && posts.length === 0) {
      postState = <div> Er zijn geen berichten gevonden. </div>;
    }

    if (posts && posts.length > 0) {
      postState = (
        <PostsContainer>
          { posts.map(post => <Post key={post.id} title={post.title.rendered} slider={post.acf.slider} body={post.acf.post} />) }
        </PostsContainer>
			);
    }	else if (error) {
      postState = <div> Er is iets misgegaan met het ophalen van de berichten. Probeer het later opnieuw. </div>;
    }

    return postState;
  }
}

export default Posts;
