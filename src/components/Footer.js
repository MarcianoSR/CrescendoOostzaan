import React from 'react';
import { Link } from 'react-router-dom';

import styled from 'styled-components';

import colors from '../colors';

const StyledFooter = styled.footer`
	padding: 20px;
	background-color: ${colors.fountainBlue};
	text-align: center;

	a {
		color: ${colors.white};
		text-decoration: none;
		text-transform: uppercase;
	}
`;

const Footer = () => (
  <StyledFooter>
    <Link to="#">Volg ons op facebook</Link>
  </StyledFooter>
);

export default Footer;
