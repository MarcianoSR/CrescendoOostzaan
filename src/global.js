/* Global styles */
import { injectGlobal } from 'styled-components';
import colors from './colors';

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,700');

  *, *:before, *:after {
    box-sizing: border-box;
  }

  body {
    margin: 0;
    font-family: 'Montserrat', 'Calibri ';
  }

	h1, h2 {
		margin: 5px 0;
	}

	p {
		color: #333333;
		font-size: 14px;
		line-height: 24px;
	}

  button, input, textarea {
    font-family: 'Montserrat', 'Calibri ';
  }

  ul {
    margin: 0;
  }

	figure {
		margin: 0;
	}

	hr {
		margin: 0;
		height: 7px;
		border-top: 0;
		background-color: ${colors.fountainBlue};
		background-image: linear-gradient(to right, ${colors.fountainBlue}, ${colors.fountainBlueLight});
	}
`;
