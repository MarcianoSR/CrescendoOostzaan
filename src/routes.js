import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import Home from './pages/Home';
import About from './pages/About';
import Footer from './components/Footer';

const Routes = () => (
  <Router>
    <div>
      <Route exact path="/" component={Home} />
      <Route path="/over-crescendo" component={About} />
      <Footer />
    </div>
  </Router>
	);

export default Routes;
