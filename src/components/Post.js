import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Title from './Title';

const PostItem = styled.div`
	background: white;
	margin-right: 20px;
	flex-basis: calc(33.33333% - 20px);
`;

const ImageContainer = styled.figure`
	max-height: 350px;
	overflow: hidden;

	img {
		width: 100%;
		height: 100%;
	}
`;

const TextContainer = styled.article`
	padding: 20px;

	.excerpt {
		margin: 0;
	}
`;

const Post = props => (
  <PostItem>
    <ImageContainer>
      <img src={props.slider[0].image.url} width="25%" alt="" />
    </ImageContainer>
    <TextContainer>
      <time>Geplaats op: 10 september</time>
      <Title heading="h2" fontSize="24">{props.title}</Title>
      <p className="excerpt" dangerouslySetInnerHTML={{ __html: props.body }} />
    </TextContainer>
  </PostItem>
);

Post.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  slider: PropTypes.arrayOf(
		PropTypes.shape({
  image: PropTypes.shape({
    url: PropTypes.string
  })
})
	).isRequired
};

export default Post;
