import React from 'react';

import colors from '../colors';
import Navigation from '../components/Navigation';

const About = props => (
  <div>
    <Navigation styles={{
      background: `linear-gradient(to right, ${colors.fountainBlue}, ${colors.fountainBlueLight});`
    }}
    />
    <h1> About... </h1>
  </div>
);

export default About;
